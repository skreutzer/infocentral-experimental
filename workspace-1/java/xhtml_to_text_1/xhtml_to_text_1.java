/* Copyright (C) 2015-2019 Stephan Kreutzer
 *
 * xhtml_to_text_1 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * xhtml_to_text_1 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with xhtml_to_text_1. If not, see <http://www.gnu.org/licenses/>.
 */

import java.io.File;
import javax.xml.stream.XMLInputFactory;
import java.io.InputStream;
import java.io.FileInputStream;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.events.XMLEvent;
import java.io.IOException;
import javax.xml.stream.XMLStreamException;
import java.io.BufferedWriter;
import java.io.OutputStreamWriter;
import java.io.FileOutputStream;
import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;


public class xhtml_to_text_1
{
    public static void main(String args[])
    {
        System.out.print("xhtml_to_text_1 Copyright (C) 2015-2019 Stephan Kreutzer\n" +
                         "This program comes with ABSOLUTELY NO WARRANTY.\n" +
                         "This is free software, and you are welcome to redistribute it\n" +
                         "under certain conditions. See the GNU Affero General Public License 3\n" +
                         "or any later version for details. Also, see the source code repositories\n" +
                         "https://gitlab.com/skreutzer/infocentral-experimental/,\n" +
                         "https://gitlab.com/publishing-systems/digital_publishing_workflow_tools/ and\n" +
                         "the project website https://infocentral.org.\n\n");

        if (args.length < 2)
        {
            System.out.println("Usage:\n\n\txhtml_to_text_1 input-file output-file\n");
            System.exit(1);
        }

        File inputFile = new File(args[0]);
        File outputFile = new File(args[1]);

        try
        {
            BufferedWriter writer = new BufferedWriter(
                                    new OutputStreamWriter(
                                    new FileOutputStream(outputFile),
                                    "UTF-8"));

            boolean isInBody = false;

            try
            {
                XMLInputFactory inputFactory = XMLInputFactory.newInstance();
                // https://www.w3.org/blog/systeam/2008/02/08/w3c_s_excessive_dtd_traffic/
                inputFactory.setProperty(XMLInputFactory.IS_SUPPORTING_EXTERNAL_ENTITIES, false);
                inputFactory.setProperty(XMLInputFactory.SUPPORT_DTD, false);
                InputStream in = new FileInputStream(inputFile);
                XMLEventReader eventReader = inputFactory.createXMLEventReader(in);

                while (eventReader.hasNext() == true)
                {
                    XMLEvent event = eventReader.nextEvent();

                    if (event.isStartElement() == true &&
                        isInBody != true)
                    {
                        if (event.asStartElement().getName().getLocalPart().equals("body") == true)
                        {
                            isInBody = true;
                        }
                    }
                    else if (event.isCharacters() == true)
                    {
                        if (isInBody == true)
                        {
                            writer.write(event.asCharacters().getData());
                        }
                    }
                    else if (event.isEndElement() == true &&
                            isInBody == true)
                    {
                        if (event.asEndElement().getName().getLocalPart().equals("body") == true)
                        {
                            isInBody = false;
                        }
                    }
                }
            }
            catch (XMLStreamException ex)
            {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
                System.exit(1);
            }
            catch (SecurityException ex)
            {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
                System.exit(1);
            }
            catch (IOException ex)
            {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
                System.exit(1);
            }

            writer.flush();
            writer.close();
        }
        catch (FileNotFoundException ex)
        {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            System.exit(1);
        }
        catch (UnsupportedEncodingException ex)
        {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            System.exit(1);
        }
        catch (IOException ex)
        {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            System.exit(1);
        }

        return;
    }
}
