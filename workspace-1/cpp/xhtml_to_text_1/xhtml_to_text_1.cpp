/* Copyright (C) 2017-2019 Stephan Kreutzer

 * xhtml_to_text_1 is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 or any later
 * version of the license, as published by the Free Software Foundation.
 *
 * xhtml_to_text_1 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with xhtml_to_text_1. If not, see <http://www.gnu.org/licenses/>.
 */

#include "XMLInputFactory.h"
#include <memory>
#include <iostream>
#include <fstream>

typedef std::unique_ptr<cppstax::XMLEventReader> XMLEventReader;
typedef std::unique_ptr<cppstax::XMLEvent> XMLEvent;

int Run(std::istream& aStreamIn, std::ostream& aStreamOut);



int main(int argc, char* argv[])
{
    std::cout << "xhtml_to_text_1 Copyright (C) 2017-2019 Stephan Kreutzer\n"
              << "This program comes with ABSOLUTELY NO WARRANTY.\n"
              << "This is free software, and you are welcome to redistribute it\n"
              << "under certain conditions. See the GNU Affero General Public License 3\n"
              << "or any later version for details. Also, see the source code repositories\n"
              << "https://gitlab.com/skreutzer/infocentral-experimental/,\n"
              << "https://gitlab.com/publishing-systems/CppStAX/ and\n"
              << "the project website https://infocentral.org.\n"
              << std::endl;

    std::unique_ptr<std::ifstream> pStreamIn = nullptr;
    std::unique_ptr<std::ofstream> pStreamOut = nullptr;

    try
    {
        if (argc >= 3)
        {
            pStreamIn = std::unique_ptr<std::ifstream>(new std::ifstream);
            pStreamIn->open(argv[1]);

            if (pStreamIn->is_open() != true)
            {
                std::cout << "xhtml_to_text_1: Couldn't open input file '" << argv[1] << "'." << std::endl;
                return -1;
            }

            pStreamOut = std::unique_ptr<std::ofstream>(new std::ofstream);
            pStreamOut->open(argv[2]);

            if (pStreamOut->is_open() != true)
            {
                std::cout << "xhtml_to_text_1: Couldn't open output file '" << argv[2] << "'." << std::endl;
                pStreamIn->close();
                return -1;
            }

            Run(*pStreamIn, *pStreamOut);

            pStreamOut->close();
            pStreamIn->close();
        }
        else
        {
            std::cout << "Usage:\n\n\txhtml_to_text_1 input-file\n" << std::endl;
        }
    }
    catch (std::exception* pException)
    {
        std::cout << "xhtml_to_text_1: Exception: " << pException->what() << std::endl;

        if (pStreamOut != nullptr)
        {
            if (pStreamOut->is_open() == true)
            {
                pStreamOut->close();
            }
        }

        if (pStreamIn != nullptr)
        {
            if (pStreamIn->is_open() == true)
            {
                pStreamIn->close();
            }
        }

        return -1;
    }

    return 0;
}

int Run(std::istream& aStreamIn, std::ostream& aStreamOut)
{
    cppstax::XMLInputFactory aFactory;
    XMLEventReader pReader = aFactory.createXMLEventReader(aStreamIn);

    bool bIsInBody = false;

    while (pReader->hasNext() == true)
    {
        XMLEvent pEvent = pReader->nextEvent();

        if (pEvent->isStartElement() == true &&
            bIsInBody != true)
        {
            if (pEvent->asStartElement().getName().getLocalPart() == "body")
            {
                bIsInBody = true;
            }
        }
        else if (pEvent->isCharacters() == true)
        {
            if (bIsInBody == true)
            {
                aStreamOut << pEvent->asCharacters().getData();
            }
        }
        else if (pEvent->isEndElement() == true &&
                 bIsInBody == true)
        {
            if (pEvent->asEndElement().getName().getLocalPart() == "body")
            {
                bIsInBody = false;
            }
        }
    }

    return 0;
}
