#!/bin/sh
# Copyright (C) 2019 Stephan Kreutzer
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License version 3 or any later
# version of the license, as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License 3 for more details.
#
# You should have received a copy of the GNU Affero General Public License 3
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# sudo apt-get install wget unzip make openjdk-8-jdk g++ texlive-latex-base texlive-latex-recommended texlive-latex-extra psutils
wget https://gitlab.com/publishing-systems/digital_publishing_workflow_tools/-/archive/master/digital_publishing_workflow_tools-master.zip -P ./java/
wget https://gitlab.com/publishing-systems/automated_digital_publishing/-/archive/master/automated_digital_publishing-master.zip -P ./java/
wget https://gitlab.com/publishing-systems/CppStAX/-/archive/master/CppStAX-master.zip -P ./cpp/
cd ./java/
unzip ./digital_publishing_workflow_tools-master.zip
mv ./digital_publishing_workflow_tools-master/ ./digital_publishing_workflow_tools/
cd ./digital_publishing_workflow_tools/
make
java -cp ./workflows/setup/setup_1/ setup_1
cd ..
unzip ./automated_digital_publishing-master.zip
mv ./automated_digital_publishing-master/ ./automated_digital_publishing/
cd ./automated_digital_publishing/
make
java -cp ./workflows/ setup1
cd ..
cd ./xhtml_to_text_1/
javac -encoding UTF-8 xhtml_to_text_1.java
cd ..
cd ..
cd ./cpp/
unzip ./CppStAX-master.zip
cp -r ./CppStAX-master/* ./xhtml_to_text_1/
rm -r ./CppStAX-master/
# cd ./xhtml_to_text_1/
# make
# g++ xhtml_to_text_1.cpp QName.o Attribute.o StartElement.o EndElement.o Characters.o Comment.o ProcessingInstruction.o XMLEvent.o XMLEventReader.o XMLInputFactory.o -o xhtml_to_text_1 -std=c++11 -Wall -Werror -Wextra -pedantic
# cd ..
cd ..
