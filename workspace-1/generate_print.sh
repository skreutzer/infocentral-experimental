#!/bin/sh
# Copyright (C) 2019 Stephan Kreutzer
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License version 3 or any later
# version of the license, as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License 3 for more details.
#
# You should have received a copy of the GNU Affero General Public License 3
# along with this program. If not, see <http://www.gnu.org/licenses/>.

mkdir -p ./temp/
rm -f ./temp/1_latex_escaped.xhtml
rm -f ./temp/2_latex_escaped.xhtml

java -cp ./java/automated_digital_publishing/txtreplace/txtreplace1/ txtreplace1 ./1.xhtml print_latex_preparation_txtreplace1_replacement_dictionary.xml ./temp/1.xhtml
printf "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<xhtml-prepare-for-latex-1-jobfile><input-file path=\"1.xhtml\"/><output-file path=\"1_latex_escaped.xhtml\"/></xhtml-prepare-for-latex-1-jobfile>\n" > ./temp/jobfile_xhtml_prepare_for_latex_1_1.xml
java -cp ./java/digital_publishing_workflow_tools/latex/xhtml_prepare_for_latex/xhtml_prepare_for_latex_1/ xhtml_prepare_for_latex_1 ./temp/jobfile_xhtml_prepare_for_latex_1_1.xml resultinfo_xhtml_prepare_for_latex_1_1.xml

java -cp ./java/automated_digital_publishing/txtreplace/txtreplace1/ txtreplace1 ./2.xhtml print_latex_preparation_txtreplace1_replacement_dictionary.xml ./temp/2.xhtml
printf "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<xhtml-prepare-for-latex-1-jobfile><input-file path=\"2.xhtml\"/><output-file path=\"2_latex_escaped.xhtml\"/></xhtml-prepare-for-latex-1-jobfile>\n" > ./temp/jobfile_xhtml_prepare_for_latex_1_2.xml
java -cp ./java/digital_publishing_workflow_tools/latex/xhtml_prepare_for_latex/xhtml_prepare_for_latex_1/ xhtml_prepare_for_latex_1 ./temp/jobfile_xhtml_prepare_for_latex_1_2.xml resultinfo_xhtml_prepare_for_latex_1_2.xml

java -cp ./java/digital_publishing_workflow_tools/xml_xslt_transformator/xml_xslt_transformator_1/ xml_xslt_transformator_1 jobfile_xml_xslt_transformator_1_latex.xml resultinfo_xml_xslt_transformator_1_latex.xml

pdflatex 1.tex
pdflatex 1.tex
pdflatex 1.tex
pdflatex 1.tex

pdf2ps 1.pdf 1.ps

psselect 1-40 1.ps 2.ps
psbook 2.ps 3.ps
pstops '2:0+1(10.5cm,0)' 3.ps 4.ps
pstops '4:0(0,14.8cm)+2,1(0,14.8cm)+3' 4.ps 5.ps
ps2pdf 5.ps 1_booklet_1.pdf
rm 2.ps
rm 3.ps
rm 4.ps
rm 5.ps

psselect 41-80 1.ps 2.ps
psbook 2.ps 3.ps
pstops '2:0+1(10.5cm,0)' 3.ps 4.ps
pstops '4:0(0,14.8cm)+2,1(0,14.8cm)+3' 4.ps 5.ps
ps2pdf 5.ps 1_booklet_2.pdf
rm 2.ps
rm 3.ps
rm 4.ps
rm 5.ps

psselect 81-118 1.ps 2.ps
psbook 2.ps 3.ps
pstops '2:0+1(10.5cm,0)' 3.ps 4.ps
pstops '4:0(0,14.8cm)+2,1(0,14.8cm)+3' 4.ps 5.ps
ps2pdf 5.ps 1_booklet_3.pdf
rm 2.ps
rm 3.ps
rm 4.ps
rm 5.ps

rm 1.ps

pdflatex 2.tex
pdflatex 2.tex
pdflatex 2.tex
pdflatex 2.tex

pdf2ps 2.pdf 1.ps

psbook 1.ps 2.ps
pstops '2:0+1(10.5cm,0)' 2.ps 3.ps
pstops '4:0(0,14.8cm)+2,1(0,14.8cm)+3' 3.ps 4.ps
ps2pdf 4.ps 2_booklet.pdf
rm 2.ps
rm 3.ps
rm 4.ps

rm 1.ps
