#!/bin/sh
# Copyright (C) 2019 Stephan Kreutzer
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License version 3 or any later
# version of the license, as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License 3 for more details.
#
# You should have received a copy of the GNU Affero General Public License 3
# along with this program. If not, see <http://www.gnu.org/licenses/>.

java -cp ./java/digital_publishing_workflow_tools/https_client/https_client_1/ https_client_1 jobfile_https_client_1_1.xml resultinfo_https_client_1_1.xml
java -cp ./java/digital_publishing_workflow_tools/https_client/https_client_1/ https_client_1 jobfile_https_client_1_1_markdown.xml resultinfo_https_client_1_1_markdown.xml
java -cp ./java/digital_publishing_workflow_tools/https_client/https_client_1/ https_client_1 jobfile_https_client_1_2.xml resultinfo_https_client_1_2.xml
java -cp ./java/digital_publishing_workflow_tools/https_client/https_client_1/ https_client_1 jobfile_https_client_1_2_markdown.xml resultinfo_https_client_1_2_markdown.xml
diff ./storage/1.xhtml ./1.xhtml >> out_sources.diff 2>&1
diff ./storage/1.md ./1.md >> out_sources.diff 2>&1
diff ./storage/2.xhtml ./2.xhtml > out_sources.diff 2>&1
diff ./storage/2.md ./2.md >> out_sources.diff 2>&1
if [ -s out_sources.diff ]
then
    "${EDITOR:-vi}" out_sources.diff
    cp ./storage/1.xhtml ./1.xhtml
    cp ./storage/2.xhtml ./2.xhtml
    cp ./storage/1.md ./1.md
    cp ./storage/2.md ./2.md
else
    rm out_sources.diff
fi

# ---------- I don't support markup declarations and the XML declaration is missing in the input as well, the W3C validator didn't complain.
# ./cpp/xhtml_to_text_1/xhtml_to_text_1 ./1.xhtml ./1.txt
# ./cpp/xhtml_to_text_1/xhtml_to_text_1 ./2.xhtml ./2.txt

java -cp ./java/xhtml_to_text_1/ xhtml_to_text_1 1.xhtml 1.txt
java -cp ./java/xhtml_to_text_1/ xhtml_to_text_1 2.xhtml 2.txt

diff ./storage/1.txt ./1.txt >> out_generated.diff 2>&1
diff ./storage/2.txt ./2.txt > out_generated.diff 2>&1
if [ -s out_generated.diff ]
then
    "${EDITOR:-vi}" out_generated.diff
    cp ./storage/1.txt ./1.txt
    cp ./storage/2.txt ./2.txt
else
    rm out_generated.diff
fi
