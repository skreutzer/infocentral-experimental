#!/bin/sh
# Copyright (C) 2019 Stephan Kreutzer
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License version 3 or any later
# version of the license, as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License 3 for more details.
#
# You should have received a copy of the GNU Affero General Public License 3
# along with this program. If not, see <http://www.gnu.org/licenses/>.

rm out_generated.diff
rm out_sources.diff

rm resultinfo_text_position_retriever_1.xml
rm resultinfo_https_client_1_2.xml
rm resultinfo_https_client_1_2_markdown.xml
rm resultinfo_https_client_1_1.xml
rm resultinfo_https_client_1_1_markdown.xml

rm 2.txt
rm 1.txt

rm 2.md
rm 1.md
rm 2.xhtml
rm 1.xhtml

rm resultinfo_xml_xslt_transformator_1_latex.xml
rm -rf ./temp/
rm 1.log
rm 1.aux
rm 1.ent
rm 1.tex
rm 2.log
rm 2.aux
rm 2.ent
rm 2.tex
